import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CreateTodoComponent} from "./components/create-todo/create-todo.component";
import {ListTodosComponent} from "./components/list-todos/list-todos.component";
import {EditTodoComponent} from "./components/edit-todo/edit-todo.component";
import {ListGroupsComponent} from "./components/list-groups/list-groups.component";
import {CreateGroupComponent} from "./components/create-group/create-group.component";

const routes: Routes = [{
  path: '', component: ListTodosComponent
}, {
  path: 'add', component: CreateTodoComponent
}, {
  path: 'edit/:id', component: EditTodoComponent
}, {
  path: 'groups', component: ListGroupsComponent
}, {
  path: 'add-group', component: CreateGroupComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TodosRoutingModule {
}
