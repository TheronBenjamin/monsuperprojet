import {Todo} from "./todo";

export interface Group {
  id: number;
  name: string,
  todos: Array<Todo>
}
