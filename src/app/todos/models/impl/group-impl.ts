import {Group} from "../group";
import {Todo} from "../todo";

export class GroupImpl implements Group {
  id!: number;
  name: string;
  todos!: Array<Todo>

  constructor(name: string) {
    this.name = name;
  }

  static fromPartial(partial: Partial<Group>): GroupImpl | null {
    if(partial.name === undefined || partial.name === null) {
      return null;
    }

    const group = new GroupImpl(partial.name);
    if (partial.id !== undefined) {
      group.id = partial.id;
    }

    if (partial.todos !== undefined) {
      group.todos = partial.todos;
    }
    return group;
  }
}
