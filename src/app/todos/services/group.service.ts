import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject, Observable, tap} from "rxjs";
// import {Todo} from "../models/todo";
import {LoaderService} from "../../core/services/loader.service";
import {Group} from "../models/group";
import {Todo} from "../models/todo";

@Injectable({
  providedIn: 'root'
})
export class GroupService {

  private url: string = "http://localhost:3000/groups/";
  private groups: BehaviorSubject<Group[]> = new BehaviorSubject<Group[]>([])


  constructor(
    private http : HttpClient,
    private loaderService : LoaderService
  ) { }

  getGroups(): Observable<Group[]> {
    this.loaderService.setLoadingState(true)
    return this.http.get<Group[]>(this.url)
      .pipe(
        // tap((data) => this.updateGroups(data)),
        tap({
          next: () => this.loaderService.setLoadingState(false),
          error: () => this.loaderService.setLoadingState(false)
        })
      );
  }

  addGroup(group: Group): Observable<Group> {
    this.loaderService.setLoadingState(true)
    return this.http.post<Group>(this.url, group)
      .pipe(
        tap(data => this.updateGroups([...this.groups.value, data])),
        tap({
          next: () => this.loaderService.setLoadingState(false),
          error: () => this.loaderService.setLoadingState(false)
        })
      );
  }

  update(group: Group): Observable<Group> {
    this.loaderService.setLoadingState(true)
    return this.http.put<Group>(this.url+group.id, group)
      .pipe(
        tap((data: Group) => {
          const groups = this.groups.value;
          const index = groups.findIndex(t => t.id === group.id);
          groups[index] = data;
          this.updateGroups(groups);
        }),
        tap({
          next: () => this.loaderService.setLoadingState(false),
          error: () => this.loaderService.setLoadingState(false)
        })
      );
  }

  private updateGroups(groups: Group[]) {
    this.groups.next(groups);
  }

}
