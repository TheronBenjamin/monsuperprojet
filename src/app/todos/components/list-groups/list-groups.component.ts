import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {Group} from "../../models/group";
import {GroupService} from "../../services/group.service";
import {TodoService} from "../../services/todo.service";
import {Todo} from "../../models/todo";

@Component({
  selector: 'app-list-groups',
  templateUrl: './list-groups.component.html',
  styleUrls: ['./list-groups.component.scss']
})
export class ListGroupsComponent implements OnInit {

  $groups: Observable<Group[]> = this.groupService.getGroups();
  $todos: Observable<Todo[]> = this.todoService.getTodos();
  panelOpenState: boolean = false;

  constructor(
    private groupService : GroupService,
    private todoService : TodoService
  ) { }

  ngOnInit(): void {
  }

}
