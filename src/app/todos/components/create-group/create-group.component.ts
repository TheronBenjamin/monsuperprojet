import { Component, OnInit } from '@angular/core';
import {TodoService} from "../../services/todo.service";
import {Observable} from "rxjs";
import {Todo} from "../../models/todo";
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {GroupService} from "../../services/group.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-create-group',
  templateUrl: './create-group.component.html',
  styleUrls: ['./create-group.component.scss']
})
export class CreateGroupComponent implements OnInit {

  $todos: Observable<Todo[]> = this.todosService.getTodos();
  form : FormGroup;

  constructor(
    private todosService : TodoService,
    private groupService : GroupService,
    private router: Router,
    private fb: FormBuilder

  ) {
    this.form = this.fb.group({
      name: this.fb.control(null, [Validators.required]),
      todos : this.fb.array([], [Validators.required])
  })
  }

  ngOnInit(): void {
  }

  submit(){
    this.groupService.addGroup(this.form.value).subscribe();
    this.router.navigate(['/groups']).catch(console.error);
  }

  getInputValue(e : any, todo : Todo) {
    const todos: FormArray = this.form.get('todos') as FormArray;
    if (e.target.checked) {
      todo.groupNames.push(this.form.value.name);
      todos.push(new FormControl(todo));
      this.todosService.update(todo);
      this.todosService.getTodos().subscribe(todos =>
        todos.forEach(todo => console.log(todo))
      );
    } else {
      const index = todos.controls.findIndex(x => x.value === e.target.value);
      todos.removeAt(index);
    }
  }

}
