import {Component, OnInit} from '@angular/core';
import {TodoService} from "../../services/todo.service";
import {Observable, switchMap} from "rxjs";
import {Todo} from "../../models/todo";
import {TodoStatus} from "../../models/todo-status";

@Component({
  selector: 'app-list-todos',
  template: `
    <ng-container *ngIf="(todoList$ | async) as todoList">
      <mat-list role="list" *ngIf="todoList.length > 0; else noTodos">
        <mat-card *ngFor="let todo of todoList" class="flex">
          <div class="flex">
            <p><b>label</b> : {{todo.label}}</p>
            <p><b>description</b> : {{todo.description}}</p>
            <p><b>date</b> : {{todo.limitDate | date}}</p>
            <b >groups: </b>
            <div *ngIf="todo.groupNames.length >0; else withoutGroup" >
                <div *ngFor="let groupName of todo.groupNames">
                  <p>{{groupName}}</p>
              </div>
            </div>
            <ng-template #withoutGroup>sans groupe</ng-template>
            <mat-form-field appearance="standard">
              <mat-label>Status</mat-label>
              <mat-select [value]="todo.status" (valueChange)="updateStatus(todo, $event)">
                <mat-option *ngFor="let status of statuses" [value]="status">
                  {{status}}
                </mat-option>
              </mat-select>
            </mat-form-field>
            <button mat-raised-button color="warn" (click)="delete(todo)">
              {{'TODOS.LIST.BUTTONS.DELETE' | translate}}
            </button>
            <button mat-raised-button color="primary" [routerLink]="['edit', todo.id]">
              {{'TODOS.LIST.BUTTONS.EDIT' | translate}}
            </button>
          </div>
          </mat-card>
      </mat-list>
      <ng-template #noTodos>
        <p>There are no todos</p>
      </ng-template>
    </ng-container>
  `
})
export class ListTodosComponent implements OnInit {
  todoList$: Observable<Todo[]> = this.todoService.todos$
  statuses: TodoStatus[] = Object.values(TodoStatus);

  constructor(
    private todoService: TodoService
  ) {
  }

  ngOnInit(): void {
    this.todoService.getTodos().subscribe();
  }

  delete(todo: Todo) {
    this.todoService.delete(todo).subscribe();
  }

  updateStatus(todo: Todo, status: TodoStatus): void {
    this.todoService.update({...todo, status}).pipe(
      switchMap(() => this.todoService.getTodos())
    ).subscribe();
  }
}
